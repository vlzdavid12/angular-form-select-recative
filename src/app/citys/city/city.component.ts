import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CityService} from '../services/city.service';
import {CitySmall, Code} from '../interface/city.interface';
import {delay, switchMap, tap} from 'rxjs/operators';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styles: []
})
export class CityComponent implements OnInit {

  miForm: FormGroup = this.fb.group({
    region: ['', Validators.required],
    citys: ['', Validators.required],
    frontier: ['', Validators.required]
  });

  loader: boolean = false;

  regions: string[] = [];
  citys: CitySmall[] = [];
  /*fronter: string[] = [];*/
  fronter: CitySmall[] = [];

  constructor(private fb: FormBuilder,
              private cityService: CityService) {
  }

  ngOnInit(): void {
    this.regions = this.cityService.regions;


    /*// Change Region
    this.miForm.get('region')?.valueChanges.subscribe((region) => {
      if (region){
        this.cityService.getCityRegion(region)
          .subscribe(city => {
            this.citys = city;
          });
      }
    });*/

    /* get region change value */
    this.miForm.get('region')?.valueChanges
      .pipe(
        tap((_) => {
          this.miForm.get('citys')?.reset('');
          this.loader = true;
        }),
        delay(1000),
        switchMap((region) => {
          return this.cityService.getCityRegion(region);
        }))
      .subscribe((resp: CitySmall[]) => {
        this.citys = resp;
        this.loader = false;
      });

    /* get cite change value */
    this.miForm.get('citys')?.valueChanges
      .pipe(
        tap(() => {
          this.fronter = [];
          this.miForm.get('frontier')?.reset('');
          this.loader = true;
        }),
        delay(1000),
        switchMap((code) => this.cityService.getCityForCode(code)),
        switchMap((city: Code | null) => this.cityService.getCityForCodes(city?.borders!))
        )
      .subscribe((res: CitySmall[])  => {this.fronter = res;  this.loader =  false });
  }

  saveForm(): void {
    console.log(this.miForm.value);
  }

}
