import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {combineLatest, Observable, of} from 'rxjs';
import {CitySmall, Code} from '../interface/city.interface';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  private _url: string = 'https://restcountries.com/v2';
  private _regions: string[] = ['Africa', 'Americas', 'Asia', 'Europe', 'Ocean'];

  get regions(): string[] {
    return [...this._regions];
  }

  constructor(private http: HttpClient) {
  }

  getCityRegion(region: string): Observable<CitySmall[]> {
    return this.http.get<CitySmall[]>(`${this._url}/region/${region}?fields=name,alpha3Code`);
  }

  getCityForCode(code: string): Observable<Code | null> {
    if (!code) {
      return of(null);
    }
    return this.http.get<Code>(`${this._url}/alpha/${code}`);
  }

  getCityForCodeSmall(code: string): Observable<CitySmall> {
    const urlCode = `${this._url}/alpha/${code}?fields?=alpha3Code;name`;
    return this.http.get<CitySmall>(urlCode);
  }

  getCityForCodes(borders: string[]): Observable<CitySmall[]> {
    if (!borders) {
      return of([]);
    }

    const getArrayPte: Observable<CitySmall>[] = [];

    borders.forEach((code) => {
      const getPte = this.getCityForCodeSmall(code);
      getArrayPte.push(getPte);
    });

    return combineLatest(getArrayPte);

  }


}
